<?php
/**
 * @file
 * phased_contest.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function phased_contest_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contest|node|phased_contest|form';
  $field_group->group_name = 'group_contest';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'phased_contest';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contest',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_contest_form',
      2 => 'field_entry_phase',
      3 => 'field_allowed_users',
      4 => 'field_number_of_entries',
      5 => 'field_pc_success_msg',
      6 => 'field_pc_success_page',
      7 => 'title',
      8 => '_add_existing_field',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_contest|node|phased_contest|form'] = $field_group;

  return $export;
}
