The Phased Contest module allows users to create contest forms with a submission
 and voting phase.

In order for entry data to be displayed on on the front end there is a custom
property for each field "Display on Entry". This allows the user to decide
what fields are displayed on the entries page.

Variables that can be set:

phased_contest_entries_limit - controls the pager on the entries listing
phased_contest_exceeded_submissions - sets the error message displayed when a
user goes over the number of submissions
phased_contest_anon_msg - sets the error message displayed when an anonymous
user attempts to submit the form that does not allow anonymous submissions


Alters available:

hook_phased_contest_entries_theme_alter - allows overriding of theme called for
 entries listing
hook_phased_contest_entry_theme_alter - allows overriding of theme called for
entry
hook_phased_contest_user_submissions_alter - allows override of number of
submissions check
hook_phased_contest_entry_data_alter - allows override of output of entry

This module contains very little theming.  The current look and feel is more
for example than actual usuage.
