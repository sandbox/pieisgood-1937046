<?php
/**
 * @file
 * phased_contest.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phased_contest_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function phased_contest_node_info() {
  $items = array(
    'phased_contest' => array(
      'name' => t('Phased Contest'),
      'base' => 'node_content',
      'description' => t('Content Type for creating contests with phases such as submission and voting'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'phased_contest_entries' => array(
      'name' => t('Phased Contest Entries'),
      'base' => 'node_content',
      'description' => t('Entries from a phased contes'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
