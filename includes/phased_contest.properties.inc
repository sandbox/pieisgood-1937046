<?php
/**
 * @file
 * Contains property forms for form_builder.
 */

/**
 * Property form form input_name for form_builder.
 */
function phased_contest_input_name_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $form['input_name'] = array(
    '#form_builder' => array('property_group' => 'default'),
    '#type' => 'textfield',
    '#title' => t('Field Name'),
    '#description' => t('This field is required to save form correctly.'),
    '#default_value' => $element['#input_name'],
    '#weight' => -1,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Property form for viewable in form_builder.
 */
function phased_contest_viewable_form(&$form_state, $form_type, $element, $property) {
  $form = array();

  $form['viewable'] = array(
    '#form_builder' => array('property_group' => 'display'),
    '#type' => 'checkbox',
    '#title' => t('Display on Entry'),
    '#description' => t('Check this if you want this field to display on entry listing'),
    '#default_value' => $element['#viewable'],
    '#weight' => -99,
  );

  return $form;
}

/**
 * Property form for allowed extensions in form_builder.
 */
function phased_contest_allowed_extensions(&$form_state, $form_type, $element, $property) {
  $form = array();

  $form['allowed_extensions'] = array(
    '#form_builder' => array('property_group' => 'validation'),
    '#type' => 'textfield',
    '#title' => t('Allowed Extensions'),
    '#description' => t('Space separated list of allowed file type extensions'),
    '#default_value' => $element['#allowed_extensions'],
    '#required' => TRUE,
    '#weight' => 1,
  );

  return $form;
}
