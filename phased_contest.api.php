<?php

/**
 * @file
 * Provides hook documentation for the VotingAPI module.
 * The theming in this module is really for example and is not considered good.
 * All themes should be overwrote.
 */

/**
 * Allows altering of what theme is used for the entries list display.
 *
 * The theme that ships with this module is incredibly rough.
 * It is highly recommended to create your own theme.
 *
 * @param string $entries_theme
 *   The name of the current theme for phased_contest on entries list.
 * @param object $node
 *   Current phased_contest node.  Passed just in case you need it.
 */
function hook_phased_contest_entries_theme_alter(&$entries_theme, &$node) {
  if ($node->nid == '123456') {
    $entries_theme = 'my_custom_entries_theme';
  }
}

/**
 * Allows altering of what theme is used to render a phased_contest_entries.
 *
 * The theme that ships with this module is incredibly rough.
 * It is highly recommended to create your own theme.
 *
 * @param string $entry_theme
 *   The name of the current theme used for entries on the entries list.
 * @param object $node
 *   Current phased_contest_entries node.  Passed just in case you need it.
 */
function hook_phased_contest_entry_theme_alter(&$entry_theme, &$node) {
  if ($node->type == 'phased_contest_entries') {
    $entry_theme = 'my_custom_entry_theme';
  }
}

/**
 * Allows altering of the number of submissions a user can make.
 *
 * Use this when you want to allow a role unlimited or limited submissions.
 * The normal value is set on the phased_contest node form.
 * See _phased_contest_user_submissions().
 *
 * @param bool $over
 *   TRUE/FALSE value that prevents the user for submitting anymore.
 */
function hook_phased_contest_user_submissions_alter(&$over) {
  global $user;
  // Allow somerole users to submit as many times as they like.
  if (in_array('somerole', $user->roles)) {
    $over = FALSE;
  }
}

/**
 * Allow altering of phased_contest_entries blob data for rendering.
 *
 * Since the form is created on the node form you might need to customize.
 * This hook allows to override the default theming of the blob field data.
 *
 * @param array $entry
 *   Blob data array of phased_contest_entries unserialized.
 * @param string $output
 *   HTML generated for for entry data.
 */
function hook_phased_contest_entry_data_alter(&$entry, &$output) {
  $output = '';
  if ($entry) {
    foreach ($entry as $key => $value) {
      // Check to see if value is viewable.
      if ($value['viewable'] == 1) {
        if (is_object($value['entry']) && isset($value['entry']->fid)) {
          // Get our file to render.
          $url = file_create_url($value['entry']->uri);
          // Build an image tag.
          $output .= theme_image(array('path' => $url, 'attributes' => array('class' => 'phased-contest-entry-image'))) . "<br />";
        }
        else {
          // Use div classes instead of brs.
          $output .= '<div class="' . $key . '">' . $value['entry'] . '</div>';
        }
      }
    }
  }
}
