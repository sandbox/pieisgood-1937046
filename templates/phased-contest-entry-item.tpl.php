<?php
/**
 * @file
 * phased-contest-entry-item.tpl.php
 *
 * Phased Contest entry item template
 */
?>
<div class="phased-contest-entry-item">
  <?php if ($entry): ?>
    <?php print $entry; ?>
  <?php endif; ?>
</div>
