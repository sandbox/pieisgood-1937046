<?php
/**
 * @file
 * phased-contest-entries.tpl.php
 *
 * Phased Contest entries template
 */
?>
<ul class="phased-contest-entries">
  <?php if ($entries): ?>
    <?php foreach($entries as $entry): ?>
      <li class="entry"><?php print $entry; ?></li>
    <?php endforeach; ?>
  <?php endif; ?>
</ul>
<?php if ($pager): ?>
  <?php print $pager; ?>
<?php endif; ?>
