<?php
/**
 * @file
 * phased-contest-total-votes.tpl.php
 *
 * Phased Contest total votes template
 */
?>
<div class="vud-widget vud-widget-updown">
  <div class="updown-score">
    <span class="updown-current-score"><?php print $count; ?></span> votes
  </div>
</div>
